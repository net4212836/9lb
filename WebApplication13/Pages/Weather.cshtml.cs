using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Text.Json;
using WebApplication13.Models;

namespace WebApplication13.Pages
{
    public class WeatherModel : PageModel
    {
		public Weather? Weather { get; private set; }

		private readonly HttpClient _httpClient;

		public WeatherModel(HttpClient httpClient)
		{
			_httpClient = httpClient;
		}

		public async Task OnPostAsync(string region)
		{
			string ApiKey = "4d3d46a279bc2d828764371c6d7d4801";
			HttpResponseMessage response = await _httpClient.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={region}&appid={ApiKey}&units=metric");

			if (response.IsSuccessStatusCode)
			{
				var content = await response.Content.ReadAsStringAsync();
				var deserializedResponse = JsonSerializer.Deserialize<WeatherApi>(content);

				if (deserializedResponse != null && deserializedResponse.main != null)
				{

					Weather = new Weather
					{
						Temp = deserializedResponse.main.temp,
						FeelsLike = deserializedResponse.main.feels_like,
						TempMin = deserializedResponse.main.temp_min,
						TempMax = deserializedResponse.main.temp_max,
						Pressure = deserializedResponse.main.pressure,
						Humidity = deserializedResponse.main.humidity
					};
					return;
				}
			}
			Weather = null;
		}
    }
}
