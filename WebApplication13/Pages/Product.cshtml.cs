using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication13.Models;

namespace WebApplication13.Pages
{
    public class ProductModel : PageModel
    {
        public List<Product> Products { get; set; } = new List<Product>() { new Product(1, "Computer", 2050) , new Product(2, "Laptop", 1000), new Product(3, "Phone", 200), new Product(4, "Microwave", 1020) };
        public void OnGet()
        {
        }
    }
}
