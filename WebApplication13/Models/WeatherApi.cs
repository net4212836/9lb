﻿namespace WebApplication13.Models
{
	public class WeatherApi
	{
		public WeatherApiResponce? main { get; set; }
	}

	public class WeatherApiResponce
	{
		public float temp { get; set; }
		public float feels_like { get; set; }
		public float temp_min { get; set; }
		public float temp_max { get; set; }
		public int pressure { get; set; }
		public int humidity { get; set; }

	}
}
