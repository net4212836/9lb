﻿namespace WebApplication13.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string? Name { get; set; }
        public int Price { get; set; }

        public Product(int ID, string Name, int Price)
        {
            this.ID = ID;
            this.Name = Name;
            this.Price = Price;
        }
    }
}
